# Leica Camera 5.0 Beta 3 for Poco F5/Redmi Note 12 Turbo (marble) AOSP 13

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/SharmagRit/vendor_xiaomi_miuicamera.git vendor/xiaomi/miuicamera
```

Make these changes in **device.mk**

```
# MiuiCamera
TARGET_INCLUDES_MIUI_CAMERA := true
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```
## Credits

### Original mod - https://github.com/a406010503/Miui_Camera

## Support

### https://t.me/itzdfplayer_stash <br>

